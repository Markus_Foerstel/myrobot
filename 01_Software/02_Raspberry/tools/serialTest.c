/*
 * serialTest.c:
 *	Very simple program to test the serial port. Expects
 *	the port to be looped back to itself
 *
 * Copyright (c) 2012-2013 Gordon Henderson. <projects@drogon.net>
 ***********************************************************************
 * This file is part of wiringPi:
 *	https://projects.drogon.net/raspberry-pi/wiringpi/
 *
 *    wiringPi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    wiringPi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with wiringPi.  If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include <wiringPi.h>
#include <wiringSerial.h>

int main ()
{
  int fd ;
  int count ;
  int i;
  unsigned int nextTime ;

  if ((fd = serialOpen ("/dev/ttyACM0", 9600/*115200*/)) < 0)
  {
    fprintf (stderr, "Unable to open serial device: %s\n", strerror (errno)) ;
    return 1 ;
  }

/*  if (wiringPiSetup () == -1)
  {
    fprintf (stdout, "Unable to start wiringPi: %s\n", strerror (errno)) ;
    return 1 ;
  }
*/
  nextTime = millis () + 300 ;

  char line[16];
  char sline[100];
  int cmdid;
  short intval1;
  short intval2;
  
  union{
  float f;
  unsigned int i;
  } val;
    
  for ( ;; )
  {

    printf ("Enter command (cmd,int,int):\n") ;	

	  fgets(sline, sizeof(sline), stdin);
	  //scanf("%[^\n]", line);
    fflush (stdout) ;
    
    cmdid   = atoi(sline);

    printf ("cmd: %d\n", cmdid) ;	
    
    char *pstr = strstr(sline, ",");
    printf ("-\n");

    if (pstr)
		{
  
      switch(cmdid)
      {  
      
      case 1:
      case 2:
      case 3:
      case 4:      
        pstr++;      
//  			printf ("str1: %s\n", pstr) ;	
  
  			intval1 = atoi(pstr);
//  			printf ("V1: %d\n", intval1) ;	
  
  			pstr = strstr(pstr, ",");
  			
  			if (pstr)
  				{
//  					printf ("str2: %s\n", pstr) ;	
  
  					intval2 = atoi(pstr+1);
//  					printf ("V2: %d\n", intval2) ;	
  				
  					line[0]=92;
  					line[1]=cmdid + 0x30;
  					line[2]=((intval1 & 0xF000) >> 12) + 0x30;
  					line[3]=((intval1 & 0x0F00) >> 8) + 0x30;
  					line[4]=((intval1 & 0x00F0) >> 4) + 0x30;
  					line[5]=(intval1 & 0x000F) + 0x30;
  					line[6]=((intval2 & 0xF000) >> 12) + 0x30;
  					line[7]=((intval2 & 0x0F00) >> 8) + 0x30;
  					line[8]=((intval2 & 0x00F0) >> 4) + 0x30;
  					line[9]=(intval2 & 0x000F) + 0x30;
  					line[10]=0;
  
  				}
          break;
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
            pstr++;                         
//      			printf ("str1: %s\n", pstr) ;	

            val.f = atof(pstr);
//  					printf ("Vf: %f\n", val.f) ;	
//  					printf ("Vh: %a\n", val.i) ;	

  					line[0]=92;
  					line[1]=cmdid + 0x30;
  					line[2]=((val.i & 0xF0000000) >> 28) + 0x30;
  					line[3]=((val.i & 0x0F000000) >> 24) + 0x30;
  					line[4]=((val.i & 0x00F00000) >> 20) + 0x30;
  					line[5]=((val.i & 0x000F0000) >> 16) + 0x30;
  					line[6]=((val.i & 0x0000F000) >> 12) + 0x30;
  					line[7]=((val.i & 0x00000F00) >> 8) + 0x30;
  					line[8]=((val.i & 0x000000F0) >> 4) + 0x30;
  					line[9]=(val.i & 0x0000000F) + 0x30;
  					line[10]=0;
      
        break;
     
      default:
        // do nothing repeat old command
        printf ("repeat previous command \n") ;	
                
        break;

      }
      
  		count = 10;
			printf ("Out: %s-%d\n", line,count) ;	
			
			serialPuts(fd, line);
			// serialPutchar (fd, line);
          				
		}

//      serialPutchar (fd, count) ;
//      nextTime += 300 ;
//      ++count ;
  }

  printf ("\n") ;
  return 0 ;
}
