#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/objdetect.hpp"
#include <iostream>
#include <unistd.h>
#include <string>

#include <X11/Xlib.h>

#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <sstream>

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

using namespace cv;
using namespace std;

#include <wiringSerial.h>
#include <pthread.h>

#define c_pi 3.141592

void drawText(Mat & image, int i, double fscale);

char ptog;
pthread_t tid1;
pthread_t tid2;
pthread_t tid3;
pthread_t tid4;

pthread_mutex_t condition_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  condition_cond  = PTHREAD_COND_INITIALIZER;

pthread_mutex_t grayimage_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  grayimage_cond  = PTHREAD_COND_INITIALIZER;

float acc_vect1_x;
float acc_vect1_y; 
float acc_vect1_x_old; 
float acc_vect1_y_old;
float acc_angle;
float angle_z;

short adist;
short headpos;

float distm;
Mat imgroomshadow;

Mat graypic;
Mat featpic;
    
extern "C" void* serialReadT(void *arg);
extern "C" void* keythread(void *arg);

extern void *imageProcT(void *arg);

/** Global variables */
String face_cascade_name = "/home/pi/opencv/opencv-3.2.0/data/haarcascades/haarcascade_frontalface_alt.xml";
String eyes_cascade_name = "/home/pi/opencv/opencv-3.2.0/data/haarcascades/haarcascade_eye_tree_eyeglasses.xml";
CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;

int thresh = 75;
int max_thresh = 255;

int newpic;

void *WinPrintWay(void *arg)
{
    float headangle;
    int gf = 100;
    
    pthread_t id = pthread_self();
    
    Mat imgroom = Mat::zeros(600, 600, CV_8UC3);

    line(imgroom, Point(550-gf-10,580), Point(560,580), Scalar( 255, 255, 255 ), 1, 8, 0 );
    line(imgroom, Point(550-gf,570), Point(550-gf,590), Scalar( 255, 255, 255 ), 1, 8, 0 );
    line(imgroom, Point(550,570), Point(550,590), Scalar( 255, 255, 255 ), 1, 8, 0 );

    putText(imgroom, "1m",
            Point(550-gf/2-10, 570),
            FONT_HERSHEY_PLAIN, 1, // font face and scale
            Scalar(255, 255, 255), // white
            1, LINE_AA); // line thickness and type
  
    int i = 0;
    int rpx, rpy;
    float cosout, sinout;
    float distx;
    float disty;
    
    while (1)
    {
  
      pthread_mutex_lock( &condition_mutex );
      pthread_cond_wait( &condition_cond, &condition_mutex );
      pthread_mutex_unlock( &condition_mutex );
  
      if (i == 0)
      {
          acc_vect1_x_old = acc_vect1_x;
          acc_vect1_y_old = acc_vect1_y;
      }
 
        distm = 0.01 * (10650.08 * pow(adist,-0.935) - 10);
        headangle = (headpos-1500) / 1500.0 * c_pi / 2.0;   // 1500 is equaviliant 90 deg

        // consider also the tilt angle of the segway
        distm = distm * cos(angle_z * c_pi / 180);

//        cout << "Head angle" << headangle * 90.0 * 2.0 / 3.141592 << endl;
//        cout << "distm     " << distm << endl;

        distx = distm * cos(headangle + c_pi);
        disty = distm * sin(headangle + c_pi);

        // rotating matrix with the accumulated alpha
        // /x'\    / cos a   -sin a \   /x\
        // |  | =  |                | * | |
        // \y'/    \ sin a   cos a  /   \y/

        cosout = cos(acc_angle);
        sinout = sin(acc_angle);

        rpx =  300 + gf * (acc_vect1_x + distx * cosout - disty * sinout);   
        rpy =  300 + gf * (acc_vect1_y + distx * sinout + disty * cosout);

        if (distm < 1.50)
        {
          circle(imgroom,
                 Point(rpx,rpy),
                 2,
                 Scalar( 255, 0, 0 ),
                 1,
                 7 );            
        }
               
        //void line(InputOutputArray img, Point pt1, Point pt2, const Scalar& color, 
        // int thickness=1, int lineType=LINE_8, int shift=0 )
        line(imgroom, 
            Point(acc_vect1_x*gf + 300,acc_vect1_y*gf + 300),
            Point(acc_vect1_x_old*gf + 300,acc_vect1_y_old*gf + 300), 
            Scalar( 0, 0, 255 ), 1, 8, 0 );
        
        rectangle(imgroom, Point(500,0), Point(600,30), Scalar(0, 100, 0),-1);

        putText(imgroom, "i; " + SSTR( i ),
            Point(510, 15),
            FONT_HERSHEY_COMPLEX, 0.5, // font face and scale
            Scalar(255, 255, 255), // white
            0.5, LINE_AA); // line thickness and type

        arrowedLine(imgroom, 
            Point(580,15),
            Point(580 - 14*cos(acc_angle),15 - 14 * sin(acc_angle)), 
            Scalar( 255, 255, 255 ), 1, 8, 0, 0.2);

        imgroomshadow = imgroom.clone();

        arrowedLine(imgroomshadow, 
            Point(acc_vect1_x*gf + 300,acc_vect1_y*gf + 300),
            Point(acc_vect1_x*gf + 300 - 14*cos(acc_angle), acc_vect1_y*gf + 300- 14 * sin(acc_angle) ), 
            Scalar( 255, 255, 255 ), 1, 8, 0, 0.2);
        
//        cout << "Received new Point" << endl;
//        cout << i << " " << acc_vect1_x << " " << acc_vect1_x_old << endl;
        
        acc_vect1_x_old = acc_vect1_x;
        acc_vect1_y_old = acc_vect1_y;
         
//       int thickness = 1;
//       int lineType = 7;
//      
//       circle( imgroom,
//               Point(200,200),
//               150+i,
//               Scalar( 0, 0, 255 ),
//               thickness,
//               lineType );            
           
//  			imshow("Robby", imgroomshadow);
//        
//  			int key = cv::waitKey(25);
//        
//      	key = (key==255) ? -1 : key; //#Solve bug in 3.2.0
//      	if (key>=0)
//      	  break;

        i++;

    }
}

int main()
{

 
    int err;
    
    ptog = 0;
    newpic = 0;

    printf ("Start Serial Reader\n");
    cout << "Built with OpenCV " << CV_VERSION << endl;

    err = pthread_create(&tid1, NULL, &keythread, NULL);
    if (err != 0){
      printf("\ncan't create thread 1:[%s]", strerror(err));
      return 1 ;
    }

    err = pthread_create(&tid2, NULL, &serialReadT, NULL);
    if (err != 0){
      printf("\ncan't create thread 2:[%s]", strerror(err));
      return 1 ;
    }

//    err = pthread_create(&tid3, NULL, &WinPrintWay, NULL);
//    if (err != 0){
//      printf("\ncan't create thread 3:[%s]", strerror(err));
//      return 1 ;
//    }

    err = pthread_create(&tid4, NULL, &imageProcT, NULL);
    if (err != 0){
      printf("\ncan't create thread 4:[%s]", strerror(err));
      return 1 ;
    }

 
     //-- 1. Load the cascades
    if( !face_cascade.load( face_cascade_name ) ){ printf("--(!)Error loading face cascade\n"); return -1; };
    if( !eyes_cascade.load( eyes_cascade_name ) ){ printf("--(!)Error loading eyes cascade\n"); return -1; };

 
    Mat image;
    VideoCapture capture;
    capture.open(0);

    if(capture.isOpened())
    {
//        namedWindow("Robby",WINDOW_AUTOSIZE);
        namedWindow("Sample",WINDOW_AUTOSIZE);
        namedWindow("Feature",WINDOW_AUTOSIZE);
        
        createTrackbar( " Canny thresh:", "Feature", &thresh, max_thresh, NULL );

        cout << "Capture is opened" << endl;
        capture.set(CAP_PROP_FPS,5);
        
        cout << "FPS " << capture.get(CAP_PROP_FPS) << endl;

        Mat graypic2;
                
        int i=0;
        for(;;)
        {
			
	      		i++;
			
            capture >> image;

            cout << "Image: " << i << endl;


            if(image.empty())
            {
                cout << "Image Empty." << endl;
                break;
            }

            pthread_mutex_lock( &grayimage_mutex );
       		cvtColor(image, graypic, COLOR_BGR2GRAY);
            
            pthread_cond_signal( &grayimage_cond );            
            pthread_mutex_unlock( &grayimage_mutex );                           

            
      			if (i % 5==0)
      			{                
                
                  graypic2 = graypic.clone();
                  
      						drawText(graypic2, i, 1.0);
                  
                  int rows = graypic2.rows / 2;
                  int cols = graypic2.cols / 2;
                                
                  line(graypic2, 
                        Point(cols -5, rows),
                        Point(cols +5, rows), 
                        Scalar( 120, 120, 120 ), 1, 8, 0 );

                  line(graypic2, 
                        Point(cols , rows - 5),
                        Point(cols , rows + 5), 
                        Scalar( 120, 120, 120 ), 1, 8, 0 );
                                   
                  if (distm < 1.5)
                  {
                    	std::string s1 = SSTR( distm );
                      putText(graypic2, ":" + s1,
                              Point(cols+5, rows+5),
                              FONT_HERSHEY_COMPLEX, 0.3, // font face and scale
                              Scalar(120, 120, 120), // white
                              1, LINE_AA); // line thickness and type                  
                  }                                   

                  imshow("Sample", graypic2);
                  
//                    imshow("Robby", imgroomshadow);
      				}

              if (newpic==1)
              {
                  cout << "Show Feature " << endl;
                  imshow("Feature", featpic);
                  newpic = 0;
              }
	    
//            cout << "imshow maybe done" << endl;
       
      			int key = cv::waitKey(10);
      			key = (key==255) ? -1 : key; //#Solve bug in 3.2.0
      			if (key>=0)
      			  break;

        }
    }
    else
    {
        cout << "No capture" << endl;
        for(;;)
        {

            WinPrintWay(NULL);

//            usleep(1000);
          
          	int key = cv::waitKey(25);
          	key = (key==255) ? -1 : key; //#Solve bug in 3.2.0
          	if (key>=0)
          	  break;

        }
    }
    return 0;
}

void drawText(Mat & image, int i, double fscale)
{
	std::string s1 = SSTR( i );
	
    putText(image, "Hello OpenCV " + s1,
            Point(20, 50),
            FONT_HERSHEY_COMPLEX, fscale, // font face and scale
            Scalar(255, 255, 255), // white
            1, LINE_AA); // line thickness and type
}
