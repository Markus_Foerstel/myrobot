

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <wiringSerial.h>
#include <pthread.h>

void* serialReadT(void *arg);
void* keythread(void *arg);

extern float acc_vect1_x;
extern float acc_vect1_y; 
extern float acc_angle;
extern short adist;
extern short headpos;
extern short angle_z;

extern char ptog;
extern pthread_mutex_t condition_mutex;
extern pthread_cond_t  condition_cond;

void* keythread(void *arg)
{
    unsigned long i = 0;
    char c;

    pthread_t id = pthread_self();

    while (1)
    {
       c = getchar();
     
       ptog = !ptog;
       printf ("Keyboard char \n");
       putchar(c);
       fflush (stdout) ;

       usleep(1000);
    }

    return NULL;
}


void* serialReadT(void *arg)
{
    unsigned long i = 0;

    int fd ;
    float tf;

    int rx_data_cnt;
    int rx_data_id;
    int rx_data_val;
   	char rx_cur;
  
    rx_data_cnt = -1;
    rx_data_id = 0;
    rx_data_val =  0;

    pthread_t id = pthread_self();

    if ((fd = serialOpen ("/dev/ttyACM0", 57600)) < 0)
    {
      printf ("Unable to open serial device: %s\n", strerror (errno)) ;
      return NULL;
    }

    while (1)
    {

      if (serialDataAvail (fd))
  	  {
        rx_cur = serialGetchar (fd);
  
        if (rx_cur == 92) // '\'
        {
          // that is the message start condition
          rx_data_cnt = 0;
          rx_data_id = 0;
          rx_data_val =  0;
        }
        else
        {
          if (rx_data_cnt == 0)
          {
              rx_data_id = rx_cur - 0x30; 
              rx_data_cnt = 1;
          }
          // A normal msg data, counter should be greater 0
          else 
          {
            if ((rx_data_cnt > 0) && (rx_data_cnt <= 8))
            {
              rx_data_val += ((unsigned long)((rx_cur-0x30) & 0x0F)) << (32 - (rx_data_cnt << 2));
      
              rx_data_cnt++;
      
              if (rx_data_cnt > 8)
              {
                rx_data_cnt = -1;
                
                switch (rx_data_id)
                {
                  case 1:
                    tf = *((float*)&rx_data_val);
                    if (ptog) printf ("Msg 1: acc_vect1_x %f\n",tf);
                    acc_vect1_x = tf;
                    break;
    
                  case 2:
                    tf = *((float*)&rx_data_val);
                    if (ptog) printf ("Msg 2: acc_vect1_y %f\n",tf);

                    acc_vect1_y = tf;           
                    pthread_cond_signal( &condition_cond );
                    pthread_mutex_unlock( &condition_mutex );                           

                    break;
    
                  case 3:
                    tf = *((float*)&rx_data_val);
                    if (ptog) printf ("Msg 3: acc_angle  %f\n",tf);
                    pthread_mutex_lock( &condition_mutex );
                    acc_angle = tf; 
                    pthread_cond_signal( &condition_cond );
                    pthread_mutex_unlock( &condition_mutex );                           
                        
                    break;
    
                  case 4:
                    tf = *((float*)&rx_data_val);
                    if (ptog) printf ("Msg 4: angle       %f\n",tf);
                    angle_z = tf; 
                    
                    break;
    
                  case 5:
                    adist = *((short*)&rx_data_val+1);
                    headpos = *((short*)&rx_data_val);
                    if (ptog) printf ("Msg 5: adistance %d\n",adist);
                    if (ptog) printf ("Msg 5: headpos   %d\n",headpos);
    
                    break;
    
                  default:
                    printf ("Msg invalid: %d\n",rx_data_id);
                  
                    break;
                 }
              }
            }
            else
             {
            		putchar (rx_cur) ;
      		      fflush (stdout) ;
                rx_data_cnt = -1;
             }
           }
         }          

      }

      usleep(100);
    }

    return NULL;
}


