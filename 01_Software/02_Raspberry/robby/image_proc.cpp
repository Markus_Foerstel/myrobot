#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/objdetect.hpp"
#include <iostream>
#include <unistd.h>
#include <string>

#include <X11/Xlib.h>

#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <sstream>

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
            ( std::ostringstream() << std::dec << x ) ).str()

using namespace cv;
using namespace std;

extern pthread_mutex_t grayimage_mutex;
extern pthread_cond_t  grayimage_cond;

extern Mat graypic;
extern Mat featpic;

extern int newpic;

extern int thresh;

void *imageProcT(void *arg)
{

    std::vector<Rect> faces;
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;



    RNG rng(12345);


    Mat edges;

    while (1)
    {
  
      pthread_mutex_lock( &grayimage_mutex );
      
      pthread_cond_wait( &grayimage_cond, &grayimage_mutex );
      pthread_mutex_unlock( &grayimage_mutex );
      
      featpic = graypic.clone(); 

      GaussianBlur(graypic, edges, Size(7,7), 1.5, 1.5);
			Canny(edges, edges, 0, thresh, 3);
      findContours( edges, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0) );

      for( size_t iz = 0; iz< contours.size(); iz++ )
         {
           Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
           drawContours( featpic, contours, (int)iz, color, 2, 8, hierarchy, 0, Point() );
         }

      cout << "Feature Calculation done" << endl;

      newpic = 1;
      
//                graypic = edges.clone();
     
//                equalizeHist( edges, edges ); 
//        			  face_cascade.detectMultiScale( edges, faces, 1.1, 2, 0|CASCADE_SCALE_IMAGE, Size(30, 30) );
//                for( size_t i = 0; i < faces.size(); i++ )
//                {
//                    Point center( faces[i].x + faces[i].width/2, faces[i].y + faces[i].height/2 );
//                    ellipse( edges, center, Size( faces[i].width/2, faces[i].height/2), 0, 0, 360, Scalar( 255, 0, 255 ), 4, 8, 0 );
//            
//                    Mat faceROI = edges( faces[i] );
//                    std::vector<Rect> eyes;
//            
//                    //-- In each face, detect eyes
//                    eyes_cascade.detectMultiScale( faceROI, eyes, 1.1, 2, 0 |CASCADE_SCALE_IMAGE, Size(30, 30) );
//            
//                    for( size_t j = 0; j < eyes.size(); j++ )
//                    {
//                        Point eye_center( faces[i].x + eyes[j].x + eyes[j].width/2, faces[i].y + eyes[j].y + eyes[j].height/2 );
//                        int radius = cvRound( (eyes[j].width + eyes[j].height)*0.25 );
//                        circle( edges, eye_center, radius, Scalar( 255, 0, 0 ), 4, 8, 0 );
//                    }
//                }
  

  }  
}