//
// Module to read data from and send to serial port
//
// V0.1
//
// The following protocol is used on the serial line.
//
//  #<cmd>,<float>;/r/n
//

#define CMD_START_CHAR '#'
#define CMD_END_CHAR ';'



#define WAIT_FOR_CMD 0
#define PROCESS_CMD 1


// how much serial data we expect before a newline
const unsigned int MAX_BUFFER_SIZE = 50;

char buffer[MAX_BUFFER_SIZE];

// ------- SER functions -----------------------------------------

void SER_Init (){
  Serial.begin (115200);
} // end of setup

/*
 * Write arbitrary string to serial port
 */
void SER_Write(const char * data)
{
  Serial.println(data);
}

/*
 * Write formatted cmd to serial port
 */
void SER_Write(const char * cmd_str, float value)
{
  #define DEC_PLACES 1000

  float val;
  int val_int;
  int val_fra;
  char buff[MAX_BUFFER_SIZE];
  char sign_char = '+';
  
  if (value < 0.0f) {
    sign_char = '-';
  }
  
  val = abs(value);
  
  val_int = (int)val;                                     // compute the integer part of the float
  val_fra = (int)((val - (float)val_int) * DEC_PLACES);   // compute 3 decimal places (and convert it to int)
  
  snprintf(buff, sizeof(buff), "%c%s,%c%d.%d%c\r", CMD_START_CHAR, cmd_str, sign_char, val_int, val_fra, CMD_END_CHAR);
  Serial.println(buff);
}  // end of SER_Write

/*
 * Process incoming serial data after a terminator received
 */
void SER_Process_data (const char * data)
 {
   // for now just echo it
   SER_Write(data);
 }  // end of SER_process_data

/*
 * Process incoming byte stream
 */
void SER_ProcessIncomingByte (const byte inByte)
{
  static char input_line[MAX_BUFFER_SIZE];
  static int cmd_state = WAIT_FOR_CMD;
  static unsigned int input_pos = 0;

  switch (inByte)
  {
    case '+':    // begin of new command
      cmd_state = PROCESS_CMD;
      break;
      
    case '\n':   // end of text
      input_line[input_pos] = 0;  // terminating null byte
  
      // terminator reached! process input_line here ...
      SER_Process_data (input_line);
  
      // reset buffer for next time
      input_pos = 0;
      cmd_state = WAIT_FOR_CMD;  
      break;
  
    case '\r':   // discard carriage return
      break;
  
    default:
      if (cmd_state = PROCESS_CMD)
      {
        // keep adding if not full ... allow for terminating null byte
        if (input_pos < (MAX_BUFFER_SIZE - 1))
        {
          input_line[input_pos++] = inByte;
        } else {
          // reset buffer for next cmd
          input_pos = 0;
          cmd_state = WAIT_FOR_CMD; 
        }
      }
      break;
  }  // end of switch

} // end of SER_ProcessIncomingByte  
  
void SER_Process()
{
  // if serial data available, process it
  while (Serial.available () > 0) {
    SER_ProcessIncomingByte (Serial.read ());
  }

}  // end of loop

