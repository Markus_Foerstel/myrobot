# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repo contains all files for the modified Self Balancing Robot.
The work is based on the commercially available self balancing from [JJRobots](https://www.jjrobots.com/)

This project aim at creating a self balancing robot carrying a Raspberry Pi plus camera 
to navigate on its own inside the house.

The focus of the project is to investigate the following fields of interest: 
- neuronal networks
- use of Arduino and raspberry Pi

### How do I get set up? ###

TBD

### Contribution guidelines ###

Please contact the author

